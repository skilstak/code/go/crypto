package crypto

import (
	"bufio"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"io"
	"math/rand"
)

func hash(text string) string {
	h := md5.New()
	h.Write([]byte(text))
	return hex.EncodeToString(h.Sum(nil))
}

// SymCrypt encrypts whatever is passed to it with the strongest
// single-passphrase encryption available.
func SymCrypt(pass string, data string) (string, error) {
	key := []byte(hash(pass))
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err := rand.Read(nonce); err != nil {
		return "", err
	}
	return string(base64.StdEncoding.EncodeToString(gcm.Seal(nonce, nonce, []byte(data), nil))), nil
}

// SymCryptInput reads one line from input as the password and
// every line after that as the content to be encrypted and passes that to
// SymCrypt. Pass it os.Stdin to read interactively.
func SymCryptInput(in io.Reader) (string, error) {
	scanner := bufio.NewScanner(in)
	scanner.Scan()
	pass := scanner.Text()
	buf := ""
	for scanner.Scan() {
		buf += scanner.Text()
	}
	if scanner.Err() != nil {
		return "", scanner.Err()
	}
	return SymCrypt(pass, buf)
}

// SymDecryptIn decrypts content that was encrypted using SymCrypt.
func SymDecrypt(pass string, encoded string) (string, error) {
	var err error
	var clear, data, key []byte
	var block cipher.Block
	var gcm cipher.AEAD
	key = []byte(hash(pass))
	block, err = aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	gcm, err = cipher.NewGCM(block)
	if err != nil {
		return "", err
	}
	data, err = base64.StdEncoding.DecodeString(encoded)
	nsize := gcm.NonceSize()
	nonce := []byte(data[:nsize])
	enc := []byte(data[nsize:])
	clear, err = gcm.Open(nil, nonce, enc, nil)
	if err != nil {
		return "", err
	}
	return string(clear), nil
}

// SymDecryptInput reads the first line of input as the password and the rest
// to be decrypted. It then passes it to SymDecrypt. Beware of extra line
// returns and such that can throw off the decryption. Pass it os.Stdin to
// read interactively.
func SymDecryptInput(in io.Reader) (string, error) {
	scanner := bufio.NewScanner(in)
	scanner.Scan()
	pass := scanner.Text()
	buf := ""
	for scanner.Scan() {
		buf += scanner.Text()
	}
	if scanner.Err() != nil {
		return "", scanner.Err()
	}
	return SymDecrypt(pass, buf)
}
