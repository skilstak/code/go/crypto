package main

import (
	"fmt"
	"os"

	"gitlab.com/skilstak/code/go/crypto"
)

func main() {
	out, err := crypto.SymCryptInput(os.Stdin)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println(out)
}
