# User-Land Encryption

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/skilstak/code/go/crypto)](https://goreportcard.com/report/gitlab.com/skilstak/code/go/crypto) [![Coverage](https://gocover.io/_badge/gitlab.com/skilstak/code/go/crypto)](https://gocover.io/gitlab.com/skilstak/code/go/crypto) [![GoDoc](https://godoc.org/gitlab.com/skilstak/code/go/crypto?status.svg)](https://godoc.org/gitlab.com/skilstak/code/go/crypto)

Practical, secure encryption, for command integration, pre-database inserts, and other flat storage based on `crypto/aes`.
