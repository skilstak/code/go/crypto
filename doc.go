// Package crypto provides practical, secure, user-land encryption that can be
// used for command integration, ad hoc content encryption for email and other
// text file storage, and pre-inserts into databases. It is easy to compose
// into other packages and test and uses crypto/AES encryption, which is
// beyond most high-level industrial and government grade encryption standards.
//
// This package includes the utility commands crypt and decrypt for doing such
// from the command line and shell scripts.
package crypto
