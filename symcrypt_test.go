package crypto_test

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/skilstak/code/go/crypto"
)

func ExampleSymCrypt() {
	pass := "longer passwords are better than crazy ones"
	enc, _ := crypto.SymCrypt(pass, "To be encrypted")
	fmt.Println(enc)
	// Output:
	// Uv38ByGCZU8WP18PAc8fy0HcAZtsYMOZzNWTGkRc54ijBCuwLohwfadstg==
}

func ExampleSymDecrypt() {
	pass := "longer passwords are better than crazy ones"
	data := "Uv38ByGCZU8WP18PAc8fy0HcAZtsYMOZzNWTGkRc54ijBCuwLohwfadstg=="
	dec, _ := crypto.SymDecrypt(pass, data)
	fmt.Println(dec)
	// Output:
	// To be encrypted
}

func TestSymDecrypt(t *testing.T) {
	cases := [][]string{
		{
			"longer passwords are better than crazy ones",
			"mmIdcpVmx00QA3xN/jlpcpnoMqVcVy87o4bnjl1c8PjaUoKulLEc/0+8jg==",
		},
		{
			"longer passwords are better than crazy ones",
			"Uv38ByGCZU8WP18PAc8fy0HcAZtsYMOZzNWTGkRc54ijBCuwLohwfadstg==",
		},
	}
	for _, args := range cases {
		dec, err := crypto.SymDecrypt(args[0], args[1])
		if err != nil {
			t.Error(err)
		}
		fmt.Printf("# %v -> %v\n", args[1], dec)
		if dec != "To be encrypted" {
			t.Error("Humm, decryption failed.")
		}
	}
}

func ExampleSymCryptInput() {
	str := "longer passwords are better than crazy ones\nTo be encrypted"
	in := strings.NewReader(str)
	enc, _ := crypto.SymCryptInput(in)
	fmt.Println(enc)
	// Output:
	// mmIdcpVmx00QA3xN/jlpcpnoMqVcVy87o4bnjl1c8PjaUoKulLEc/0+8jg==
}

func ExampleSymDecryptInput() {
	str := "longer passwords are better than crazy ones\nmmIdcpVmx00QA3xN/jlpcpnoMqVcVy87o4bnjl1c8PjaUoKulLEc/0+8jg=="
	in := strings.NewReader(str)
	enc, _ := crypto.SymDecryptInput(in)
	fmt.Println(enc)
	// Output:
	// To be encrypted
}
